#![link(name = "felix", author="David J Felix", vers = "0.0.1")]

use std::collections::BitVec;
use num::rational::Ratio;
use core::num::FromPrimitive;
use std::usize;

pub struct Felix1 {
    state: BitVec
}

impl Felix1 {
    pub fn count_neg(&self) -> usize {
        let frac = self.frac();
        *(frac.denom()) - *(frac.numer())
    }
    
    pub fn count_pos(&self) -> usize {
        let frac = &self.frac();
        *(frac.numer())
    }
    
    pub fn count_tot(&self) -> usize {
        let frac = &self.frac();
        *(frac.denom())
    }
    
    pub fn frac(&self) -> Ratio<usize> {
        let numer: usize = 0;
        let denom: usize = 0;
        Ratio::new(numer, denom)
        
    }
    
    /*pub fn generate(&self, size: usize) -> BitVec {
    }*/
    
    pub fn mode(&self) -> bool {
        // The mode is equal to the most significant bit
        //self.state.get(self.state.len() - 1)
        true //FIXME
    }
    
    pub fn new() -> Felix1 {
        Felix1 {
            state: BitVec::with_capacity(usize::BITS)
        }
    }
    
    pub fn prob(&self) -> f64 {
        let frac = &self.frac();
        //f64::from_uint(*frac.numer()) / f64::from_uint(*frac.denom())
        0.0 //FIXME
    }
    
    pub fn sample(&self, value: bool) {
    }
}
