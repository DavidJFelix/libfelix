/* felix1.c
 *
 * Copyright (c) 2011-2014 David J Felix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "felix1.h"

/* destroy_f1e
 * This function destorys an estimator by freeing the heap memory that it 
 * resides in.
 */
inline void destroy_f1e(felix1 *estimator_ptr) {
	free(estimator_ptr->state);
	free(estimator_ptr);
}

/* gen_f1e
 * This function will use the estimator passed to generate
 * values. It generates these values based on probability
 * and the weight of the estimator
 */
_Bool gen_f1e(const felix1 estimator, _Bool *val) {
	register ulong_t state;
	register ulong_t use_mask;
	register ulong_t rand_mask;
	unsigned char rand[sizeof(ulong_t)];
	uint8_t *state_ptr8;
	uint16_t *state_ptr16;
	uint32_t *state_ptr32;
	uint64_t *state_ptr64;
	uint8_t size;
	int shift;

	size = estimator.size;
	
	// Make sure that size isn't goofy.
	if(unlikely(size == 0)) {
		return false;
	}

	// Make sure that RAND_bytes can generate enough randomness
	if(unlikely(RAND_bytes(rand, ULONG_BYTE) == 0)) {
		return false;
	}

	// Fill a pointer thats size corresponds to the estimator's size
	switch (size) {
		case UINT8_BYTE:
			state_ptr8 = estimator.state;
			state = (ulong_t)(*state_ptr8);
			break;
		case UINT16_BYTE:
			state_ptr16 = estimator.state;
			state = (ulong_t)(*state_ptr16);
			break;
		case UINT32_BYTE:
			state_ptr32 = estimator.state;
			state = (ulong_t)(*state_ptr32);
			break;
		case UINT64_BYTE:
			state_ptr64 = estimator.state;
			state = (ulong_t)(*state_ptr64);
			break;
		default: // Fail if the size isn't a correct value
			return false;
	}

	use_mask = state;

	// Make sure the state isn't goofy
	if (unlikely(use_mask == 0)) {
		return false;
	}

	// Perform a BSR on use_mask
	shift = ULONG_BIT - clzl(use_mask) - 1;

	// Remove the left use flag from the state and set use_mask
	use_mask &= (1 << shift);
	state ^= use_mask;
	use_mask -= 1;

	// Generate the value
	rand_mask = use_mask & ((ulong_t)*(rand));
	if (rand_mask > state) {
		*val = false;
	}
	else { // rand_mask <= state
		if (likely(rand_mask < state)) {
			*val = true;
		}
		else { // rand_mask == state
			// 50/50 distribution tiebreaker
			*val = (rand_mask & 1) ? true : false;
		}
	}

	return true;
}


/* get_f1e_count_neg
 * This function wraps the get_f1e_frac function and attempts to inline
 * the extra effort of calculating the count of negative observances.
 */
inline _Bool get_f1e_count_neg(const felix1 estimator, ulong_t *count) {
	ulong_t *numer;
	ulong_t *denom;
	_Bool fun_rtn;
	
	numer = malloc(sizeof(ulong_t));
	denom = malloc(sizeof(ulong_t));
	fun_rtn = get_f1e_frac(estimator, numer, denom);
	
	if (fun_rtn) {
		*count = *denom - *numer;
	}
	
	free(numer);
	free(denom);
	return fun_rtn;
}


/* get_f1e_count_pos
 * This function wraps the get_f1e_frac function and attempts to inline
 * the extra effort of calculating the count of positive observances.
 */
inline _Bool get_f1e_count_pos(const felix1 estimator, ulong_t *count) {
	ulong_t *numer;
	ulong_t *denom;
	_Bool fun_rtn;
	
	numer = malloc(sizeof(ulong_t));
	denom = malloc(sizeof(ulong_t));
	fun_rtn = get_f1e_frac(estimator, numer, denom);
	
	if (fun_rtn) {
		*count = *numer;
	}
	
	free(numer);
	free(denom);
	return fun_rtn;
}

/* get_f1e_count_tot
 * This function wraps the get_f1e_frac function and attempts to inline
 * the extra effort of calculating the count of total observances.
 */
inline _Bool get_f1e_count_tot(const felix1 estimator, ulong_t *count) {
	ulong_t *numer;
	ulong_t *denom;
	_Bool fun_rtn;
	
	numer = malloc(sizeof(ulong_t));
	denom = malloc(sizeof(ulong_t));
	fun_rtn = get_f1e_frac(estimator, numer, denom);
	
	if (fun_rtn) {
		*count = *denom;
	}
	
	free(numer);
	free(denom);
	return fun_rtn;
}

/* get_f1e_frac
 * This function attempts to get the fractional value of a felix1 estimator.
 * If it succeeds, it will return true, if it fails, false. The variables for
 * numer and denom are passed by reference, so their values are changed in
 * line.
 */
bool get_f1e_frac(const felix1 estimator, ulong_t *numer, ulong_t *denom) {
	register ulong_t state;
	register ulong_t use_mask;
	uint8_t *state_ptr8;
	uint16_t *state_ptr16;
	uint32_t *state_ptr32;
	uint64_t *state_ptr64;
	uint8_t size;
	int shift;

	size = estimator.size;

	// Make sure that size isn't goofy
	if (unlikely(size == 0)) {
		return false;
	}

	// Fill a pointer thats size corresponds to the estimator's size
	switch (size) {
		case UINT8_BYTE:
			state_ptr8 = estimator.state;
			state = (ulong_t)(*state_ptr8);
			break;
		case UINT16_BYTE:
			state_ptr16 = estimator.state;
			state = (ulong_t)(*state_ptr16);
			break;
		case UINT32_BYTE:
			state_ptr32 = estimator.state;
			state = (ulong_t)(*state_ptr32);
			break;
		case UINT64_BYTE:
			state_ptr64 = estimator.state;
			state = (ulong_t)(*state_ptr64);
			break;
		default: // Fail if the size isn't a correct value
			return false;
	}

	use_mask = state;

	// Make sure the state isn't goofy
	if (unlikely(use_mask == 0)) {
		return false;
	}

	// Perform a BSR on use_mask
	shift = ULONG_BIT - clzl(use_mask) - 1;

	// Remove the left use flag from the state and set use_mask
	use_mask &= (1 << shift);
	state ^= use_mask;
	use_mask -= 1;

	// Perform some awesome bit magic to generate numer/denom.
	// This property ought to be explained in the docs
	state <<= 1;
	state += 1;
	*numer = state;
	use_mask += 1;
	use_mask <<= 1;
	*denom = use_mask;

	return true;
}

/* get_f1e_mode
 * This function will return the observed mode of a felix 1 estimator.
 * Since felix 1 estimators only observe true or false, this will be
 * a 1 or a 0, corresponding to which has is more probable.
 */
_Bool get_f1e_mode(const felix1 estimator, _Bool *val) {
	register ulong_t state;
	register ulong_t use_mask;
	uint8_t *state_ptr8;
	uint16_t *state_ptr16;
	uint32_t *state_ptr32;
	uint64_t *state_ptr64;
	uint8_t size;
	int shift;
	
	size = estimator.size;
	
	// Make sure that size isn't goofy
	if (unlikely(size == 0)) {
		return false;
	}
	
	switch (size) {
		case UINT8_BYTE:
			state_ptr8 = estimator.state;
			state = (ulong_t)(*state_ptr8);
			break;
		case UINT16_BYTE:
			state_ptr16 = estimator.state;
			state = (ulong_t)(*state_ptr16);
			break;
		case UINT32_BYTE:
			state_ptr32 = estimator.state;
			state = (ulong_t)(*state_ptr32);
			break;
		case UINT64_BYTE:
			state_ptr64 = estimator.state;
			state = (ulong_t)(*state_ptr64);
			break;
		default: // Fail if the size isn't a correct value
			return false;
	}

	use_mask = state;

	// Make sure the state isn't goofy
	if (unlikely(use_mask == 0)) {
		return false;
	}

	// Perform a BSR on use_mask
	shift = ULONG_BIT - clzl(use_mask) - 1;

	// Remove the left use flag from the state and set use_mask
	use_mask &= (1 << shift);
	state ^= use_mask;
	use_mask -= 1;
	
	// The mode is equal to the most significant bit
	// TODO: remove ternary here in favor of a cast after refactor
	*val = state & (use_mask >> 1) ? true : false;
	return true;
}

/* get_f1e_prob
 * This function wraps the get_f1e_frac function and attempts to inline
 * the extra effort of calculating the probability that the fraction
 * rerpresents.
 */
inline _Bool get_f1e_prob(const felix1 estimator, double *prob) {
	ulong_t *numer;
	ulong_t *denom;
	_Bool fun_rtn;

	numer = malloc(sizeof(ulong_t));
	denom = malloc(sizeof(ulong_t));
	fun_rtn = get_f1e_frac(estimator, numer, denom);

	// Don't try to perform the probabiblity check if the function failed
	if (fun_rtn) {
		*prob = ((double)*numer) / ((double)*denom);
	}
	
	free(numer);
	free(denom);
	return fun_rtn;
}

/* init_f1e
 * This function is a constructor. It will create a felix1 estimator on
 * the heap, initialize its values to something sane and then return the
 * address of the estimator.
 */
inline felix1* init_f1e() {
	felix1* new_f1e_ptr;
	size_t f1e_size;
	uint8_t *state_ptr8;

	f1e_size = sizeof(felix1);
	new_f1e_ptr = malloc(f1e_size);
	new_f1e_ptr->state = malloc(UINT8_BYTE);
	state_ptr8 = new_f1e_ptr->state;
	*state_ptr8 = 1;
	new_f1e_ptr->size = UINT8_BYTE;
	return new_f1e_ptr;
}

/* samp_f1e
 * This function "samples" the felix 1 estimator, which means that it
 * provides actual observed data and has the felix 1 estimator modify itself
 * based on the observed data.
 */
_Bool samp_f1e(felix1 *estimator, const _Bool val) {
	register ulong_t state;
	register ulong_t use_mask;
	register ulong_t rand_mask;
	unsigned char rand[sizeof(ulong_t)];
	uint8_t *state_ptr8;
	uint16_t *state_ptr16;
	uint32_t *state_ptr32;
	uint64_t *state_ptr64;
	uint8_t size;
	int shift;

	size = estimator->size;
	
	// Make sure that size isn't goofy; fix it if it is.
	if(unlikely(size == 0)) {
		estimator->size = 1;
		free(estimator->state);
		estimator->state = malloc(UINT8_BYTE);
		state_ptr8 = estimator->state;
		*state_ptr8 = 1;
		state_ptr8 = NULL;
	}

	// Make sure that RAND_bytes can generate enough randomness
	if(unlikely(RAND_bytes(rand, ULONG_BYTE) == 0)) {
		return false;
	}

	// Fill a pointer thats size corresponds to the estimator's size
	switch (size) {
		case UINT8_BYTE:
			state_ptr8 = estimator->state;
			state = (ulong_t)(*state_ptr8);
			break;
		case UINT16_BYTE:
			state_ptr16 = estimator->state;
			state = (ulong_t)(*state_ptr16);
			break;
		case UINT32_BYTE:
			state_ptr32 = estimator->state;
			state = (ulong_t)(*state_ptr32);
			break;
		case UINT64_BYTE:
			state_ptr64 = estimator->state;
			state = (ulong_t)(*state_ptr64);
			break;
		default: // Fail if the size isn't a correct value
			return false;
	}

	use_mask = state;

	// Make sure the state isn't goofy.
	if (unlikely(use_mask == 0)) {
		return false;
	}

	// Perform a BSR on use_mask
	shift = ULONG_BIT - clzl(use_mask) - 1;

	// Remove the left use flag from the state and set use_mask
	use_mask &= (1 << shift);
	state ^= use_mask;
	use_mask -= 1;

	rand_mask = use_mask & (*(ulong_t *)(rand));
	if (unlikely(rand_mask == state)) {

		// Check if the estimator is at its maximum length
		if (use_mask == F1E_MAX) {

			// Ensure that the estimator will not underlflow
			if ((likely(state != 0)) || (val == true)) {

				// Ensure that the estimator will not overflow
				if ((likely(state != use_mask)) || (val == false)) {
					state += val ? 1 : -1;
				}
			}
		}
		else { // use_mask != F1E_MAX, we can expand

			// Ensure that the estimator will not underflow
			if ((likely(state != 0)) || (val == true)) {
				state <<= 1;
				state += val ? 1 : -1;
			}

			// Regardless, expand.
			use_mask <<= 1;
			use_mask += 1;
		}
	}
	else { // rand_mask != state; likely
		if (rand_mask > state) {

			// Only modify the estimator if val is true
			if (val == true) {

				// Ensure that the estimator will not overflow
				if (likely(state != use_mask)) {
					state += 1;
				}
			}
		}
		else { // rand_mask < state;

			//Only modify the estimator if val is false
			if (val == false) {

				// Ensure that the estimator will not underflow
				if (likely(state != 0)) {
					state -= 1;
				}
			}
		}
	}

	// Adjust state to how it will be stored in memory
	use_mask += 1;
	state |= use_mask;

	// Check if there needs to be reallocation by determining the new size
	shift = ULONG_BIT - clzl(use_mask);
	if (likely(shift > 8)) {
		if (likely(shift > 16)) {
			if (shift > 32) {
				size = UINT64_BYTE;
			}
			else { // 16 < shift <= 32
				size = UINT32_BYTE;
			}
		}
		else { // 8 < shift <= 16
			size = UINT16_BYTE;
		}
	}
	else { // shift <= 8
		size = UINT8_BYTE;
	}

	// Perform new memory allocation, old memory deallocation and pointer assignment.
	switch (size) {
		case UINT8_BYTE:
			state_ptr8 = malloc(UINT8_BYTE);
			estimator->size = size;
			free(estimator->state);
			estimator->state = state_ptr8;
			*state_ptr8 = (uint8_t)state;
			break;
		case UINT16_BYTE:
			state_ptr16 = malloc(UINT16_BYTE);
			estimator->size = size;
			free(estimator->state);
			estimator->state = state_ptr16;
			*state_ptr16 = (uint16_t)state;
			break;
		case UINT32_BYTE:
			state_ptr32 = malloc(UINT32_BYTE);
			estimator->size = size;
			free(estimator->state);
			estimator->state = state_ptr32;
			*state_ptr32 = (uint32_t)state;
			break;
		case UINT64_BYTE:
			state_ptr64 = malloc(UINT64_BYTE);
			estimator->size = size;
			free(estimator->state);
			estimator->state = state_ptr64;
			*state_ptr64 = (uint64_t)state;
			break;
		default: // Serious issues.
			return false;
	}

	return true;
}
