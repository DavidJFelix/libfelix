/* felix2.c
 *
 * Copyright (c) 2011-2014 David J Felix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "felix2.h"

bool samp_f2e(felix2 *estimator, const char *val, const size_t len_val) {
	//Bring in the bytes and start doing permutation sampling
	//permutation sampling means to go to the node for each bit of val
	//traveling to that node's children (if they exist) and sampling
	//if a sample roll succeeds, all children get a chance to roll
	//this continues until total roll failure
	return EXIT_FAILURE;
}

bool gen_f2e(const felix2 *estimator, char *val, const size_t len_val) {
	return EXIT_FAILURE;
}

void get_f2e_avrg() {
}

void get_f2e_distr() {
}
