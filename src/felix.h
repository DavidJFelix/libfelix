/* felix1.h
 *
 * Copyright (c) 2011-2014 David J Felix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef FELIX_H
#define FELIX_H


#include <openssl/rand.h>
#ifndef FELIX_MACRO_DEPS
#define FELIX_MACRO_DEPS
	#include <limits.h>
	#include <stdint.h>
	#include <stdlib.h>
	#include <string.h>
	#include <stdbool.h>
#endif

// Define sizes
// TODO: switch from define blocks to const
#define BYTE_BIT 8
#define UINT8_BIT 8
#define UINT8_BYTE UINT8_BIT/BYTE_BIT
#define UINT16_BIT 16
#define UINT16_BYTE UINT16_BIT/BYTE_BIT
#define UINT32_BIT 32
#define UINT32_BYTE UINT32_BIT/BYTE_BIT
#define UINT64_BIT 64
#define UINT64_BYTE UINT64_BIT/BYTE_BIT
#define ULONG_BIT __WORDSIZE
#define ULONG_BYTE ULONG_BIT/BYTE_BIT

// Felix 1 Estimators need the MSB to hold leading 1 so they are restricted
// to maximum allowable size (in this case we use register size, ulong)
#define F1E_MAX ULONG_MAX >> 1

// Define some compiler optimization macros for path prediction and advanced
// instruction set use.
#define clzl(x) __builtin_clzl((x))
#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)
#define fast_true(x) __builtin_expect((x), 1)
#define fast_false(x) __builtin_expect((x), 0)


typedef unsigned long ulong_t;

#endif // FELIX_H
